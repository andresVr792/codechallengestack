import unittest
from data import Data
import re

class TestDats(unittest.TestCase):
    def setUp(self): self.data = Data()

    def test_listResultLengh(self):
        self.assertEqual(len(self.data.listResult), 30)

    def test_listResultHasAtributes(self):
        list(map(lambda x:self.assertEqual(len(x),4),self.data.listResult))

    def test_titleHaslessEqualsFourWords(self):
        list(map(lambda x: self.assertTrue(len(re.findall('[\w]+',x[1])) <=4), self.data.filterLessEqualFourWords(self.data.listResult)))

    def test_titleHasMoreFourWords(self):
        list(map(lambda x: self.assertTrue(len(re.findall('[\w]+',x[1])) >4), self.data.filterMoreFourWords(self.data.listResult)))


if __name__ == '__main__':
    unittest.main()