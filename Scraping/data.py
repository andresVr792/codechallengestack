import requests
import re
from bs4 import BeautifulSoup

class Data:
    def __init__(self):
        self.targetPage= requests.get("https://news.ycombinator.com")
        self.contentPage= BeautifulSoup(self.targetPage.content, 'html.parser')
        self.orderList=list(self.toString(self.contentPage.find_all(class_='rank')))
        self.titleList = list(self.toString(self.contentPage.find_all('a', class_='storylink')))
        self.scoreList = list(self.filteringPatern(self.contentPage.find_all('td', class_='subtext'), '(\d{1,})\s[^hmdc]'))
        self.commentsList = list(self.filteringPatern(self.contentPage.find_all('td', class_='subtext'), '.\s(\d{1,})\s[^hdp]'))
        self.listResult=list(zip(self.orderList,self.titleList,self.scoreList,self.commentsList))

    def toString(self,list):
        return map(lambda item:item.get_text(),list)

    def filteringPatern(self,list,pattern):
        return map(lambda item:int(re.findall(pattern,item.get_text())[0]) if re.findall(pattern,item.get_text()) else 0,list)

    def filterLessEqualFourWords(self,listResult):
        return list(filter(lambda news:len(re.findall('[\w]+',news[1]))<=4,listResult))

    def filterMoreFourWords(self,listResult):
        return list(filter(lambda news:len(re.findall('[\w]+',news[1]))>4,listResult))

    def filterLessEqualFourWordsOrdered(selfself, list):
        return sorted(list,key=lambda item:item[2])

    def filterMoreFourWordsOrdered(self,list):
        return sorted(list, key=lambda item: item[3])

#scraping=Data()


#listNewsResult=scraping.listResult

#filterLessEqualFourWords=scraping.filterLessEqualFourWords(listNewsResult)

#filterMoreFourWords=scraping.filterMoreFourWords(listNewsResult)

#filterLessEqualFourWordsOrdered=scraping.filterLessEqualFourWordsOrdered(filterLessEqualFourWords)

#filterMoreFourWordsOrdered=scraping.filterMoreFourWordsOrdered(filterMoreFourWords)

#print(list(result))
#print (filterLessEqualFourWordsOrdered)
#print('................................')
#print (filterMoreFourWordsOrdered)
#print (filterMoreFourWords)
